# statemachine-macro

A Rust crate providing a macro to easily create state machines. Documentation is available on [docs.rs](https://docs.rs/statemachine-macro).

## Usage

Add this to your `Cargo.toml`:
```toml
[dependencies]
statemachine-macro = "0.1"
```

## Getting Started

```rs
use statemachine_macro::*;

statemachine! {
    struct Foo;

    enum FooState consumes [char] from Start accepts [NonEmpty];

    Start => {
        char match _ => NonEmpty
    },

    NonEmpty => {
        _ => NonEmpty
    }
}

fn main() {
    let mut foo: Foo = statemachine_new!(Foo{});
    assert!(!foo.is_accepting());
    foo.consume('a');
    assert!(foo.is_accepting());
    foo.consume('b');
    assert!(foo.is_accepting());
    foo.consume('c');
    assert!(foo.is_accepting());
}
```

## License

This crate is published under the terms of the MIT license. See the LICENSE file for details.