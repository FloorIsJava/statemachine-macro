use statemachine_macro::*;

statemachine! {
    struct Foo;

    enum FooState consumes [char] from Start accepts [NonEmpty];

    Start => {
        char match _ => NonEmpty
    },

    NonEmpty => {
        _ => NonEmpty
    }
}

#[test]
fn can_create() {
    let _foo: Foo = statemachine_new!(Foo{});
}

#[test]
fn not_accepting_when_new() {
    let foo: Foo = statemachine_new!(Foo{});
    assert!(!foo.is_accepting());
}

#[test]
fn can_reset() {
    let mut foo: Foo = statemachine_new!(Foo{});
    foo.reset(FooState::Start);
    assert!(!foo.is_accepting());
    foo.reset(FooState::NonEmpty);
    assert!(foo.is_accepting());
}

#[test]
fn can_accept_words() {
    let mut foo: Foo = statemachine_new!(Foo{});
    assert!(!foo.is_accepting());
    foo.consume('a');
    assert!(foo.is_accepting());
    foo.consume('b');
    assert!(foo.is_accepting());
    foo.consume('c');
    assert!(foo.is_accepting());
}