use statemachine_macro::*;

statemachine! {
    struct Foo;

    enum FooState consumes [()] from Start;
}

#[test]
fn can_create() {
    let _foo: Foo = statemachine_new!(Foo{});
}

#[test]
fn not_accepting() {
    let foo: Foo = statemachine_new!(Foo{});
    assert!(!foo.is_accepting());
}

#[test]
fn can_reset() {
    let mut foo: Foo = statemachine_new!(Foo{});
    foo.reset(FooState::Start);
}

#[test]
#[should_panic]
fn can_accept_empty() {
    let mut foo: Foo = statemachine_new!(Foo{});
    // Panics due to transition exhaustion
    foo.consume(());
}