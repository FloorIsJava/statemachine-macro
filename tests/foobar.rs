use statemachine_macro::*;

statemachine! {
    struct Foo;

    enum FooState consumes [char] from Start accepts [WroteFoobar];

    Start => {
        char match 'f' => WroteF,
        _ => Error
    },

    WroteF => {
        char match 'o' => WroteFo,
        _ => Error
    },

    WroteFo => {
        char match 'o' => WroteFoo,
        _ => Error
    },

    WroteFoo => {
        char match 'b' => WroteFoob,
        _ => Error
    },

    WroteFoob => {
        char match 'a' => WroteFooba,
        _ => Error
    },

    WroteFooba => {
        char match 'r' => WroteFoobar,
        _ => Error
    },

    WroteFoobar => {
        _ => Error
    },

    Error => {
        _ => Error
    }
}

#[test]
fn can_create() {
    let _foo: Foo = statemachine_new!(Foo{});
}

#[test]
fn not_accepting_when_new() {
    let foo: Foo = statemachine_new!(Foo{});
    assert!(!foo.is_accepting());
}

#[test]
fn not_accepting_prefix() {
    let mut foo: Foo = statemachine_new!(Foo{});
    assert!(!foo.is_accepting());
    foo.consume('f');
    assert!(!foo.is_accepting());
    foo.consume('o');
    assert!(!foo.is_accepting());
    foo.consume('o');
    assert!(!foo.is_accepting());
    foo.consume('b');
    assert!(!foo.is_accepting());
    foo.consume('a');
    assert!(!foo.is_accepting());
}

#[test]
fn can_accept_foobar() {
    let mut foo: Foo = statemachine_new!(Foo{});
    foo.consume('f');
    foo.consume('o');
    foo.consume('o');
    foo.consume('b');
    foo.consume('a');
    foo.consume('r');
    assert!(foo.is_accepting());
}

#[test]
fn not_accepting_foobar_plus_suffix() {
    let mut foo: Foo = statemachine_new!(Foo{});
    foo.consume('f');
    foo.consume('o');
    foo.consume('o');
    foo.consume('b');
    foo.consume('a');
    foo.consume('r');
    assert!(foo.is_accepting());
    foo.consume('x');
    assert!(!foo.is_accepting());
    foo.consume('f');
    foo.consume('o');
    foo.consume('o');
    foo.consume('b');
    foo.consume('a');
    foo.consume('r');
    assert!(!foo.is_accepting());
}

#[test]
fn accepting_foobar_after_reset() {
    let mut foo: Foo = statemachine_new!(Foo{});
    foo.consume('f');
    foo.consume('o');
    foo.consume('o');
    foo.consume('x');
    assert!(!foo.is_accepting());
    foo.reset(FooState::Start);
    foo.consume('f');
    foo.consume('o');
    foo.consume('o');
    foo.consume('b');
    foo.consume('a');
    foo.consume('r');
    assert!(foo.is_accepting());
}