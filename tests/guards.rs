use statemachine_macro::*;

statemachine! {
    struct Foo;

    enum FooState consumes [char] from Start accepts [Start];

    Start => {
        char match 'f' => if false Start,
        char match 'g' => if panic!("oh wait!") Start,
        char match 'h' => if false Error,
        char match 'h' => if true Start,
        _ => Error
    },

    Error => {}
}

#[test]
fn can_create() {
    let _foo: Foo = statemachine_new!(Foo{});
}

#[test]
fn guard_blocks_transition() {
    let mut foo: Foo = statemachine_new!(Foo{});
    assert!(foo.is_accepting());
    foo.consume('f');
    assert!(!foo.is_accepting());
}

#[test]
#[should_panic]
fn guard_evaluates_panic() {
    let mut foo: Foo = statemachine_new!(Foo{});
    assert!(foo.is_accepting());
    foo.consume('g');
}

#[test]
fn true_guard_evaluated() {
    let mut foo: Foo = statemachine_new!(Foo{});
    assert!(foo.is_accepting());
    foo.consume('h');
    assert!(foo.is_accepting());
}