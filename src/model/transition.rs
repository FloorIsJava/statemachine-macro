use quote::{quote, ToTokens};
use syn::{parse, Token};

pub struct Transition {
    pub guard: Option<syn::Expr>,
    pub raw: RawTransition
}

pub enum RawTransition {
    CatchAll(syn::Expr),
    CatchType(syn::Type, syn::Pat, syn::Expr)
}

// To token conversion
impl Transition {
    pub fn get_arm(&self, symbols_name: &syn::Ident, sym_types: &Vec<syn::Type>) -> proc_macro2::TokenStream {
        let pattern = if let RawTransition::CatchType(ty, pat, _) = &self.raw {
            // TODO! where do we get the variant? Type-Eq?
            let ident = Self::find_type_ident(sym_types, ty);
            quote! { #symbols_name::#ident(#pat) }
        } else {
            quote! { _ }
        };

        let guard = if let Some(guard) = &self.guard {
            quote! { if #guard }
        } else {
            quote! {}
        };

        let expr = match &self.raw {
            RawTransition::CatchAll(expr) => quote! { #expr },
            RawTransition::CatchType(_, _, expr) => quote! { #expr }
        };

        quote! {
            #pattern #guard => #expr
        }
    }

    fn find_type_ident(types: &Vec<syn::Type>, ty: &syn::Type) -> syn::Ident {
        let idx = types.iter().enumerate()
            .find(|(_, x)| Self::type_eq(x, ty))
            .map(|(i, _)| i);
        if let Some(i) = idx {
            syn::Ident::new(&format!("V{}", i), proc_macro2::Span::call_site())
        } else {
            panic!("No such type in symbol list!");
        }
    }

    fn type_eq(a: &syn::Type, b: &syn::Type) -> bool {
        // Not quite real eq, but good enough.
        let a_str = format!("{}", a.to_token_stream());
        let b_str = format!("{}", b.to_token_stream());
        a_str == b_str
    }
}

// Parsing
impl Transition {
    fn parse_guard(buf: parse::ParseStream) -> syn::Result<Option<syn::Expr>> {
        if buf.peek(Token![if]) {
            buf.parse::<Token![if]>()?;
            Ok(Some(buf.parse()?))
        } else {
            Ok(None)
        }
    }
}

impl parse::Parse for Transition {
    fn parse(buf: parse::ParseStream) -> syn::Result<Self> {
        let guard;
        let raw;
        if buf.peek(Token![_]) {
            buf.parse::<Token![_]>()?;
            buf.parse::<Token![=>]>()?;
            guard = Self::parse_guard(&buf)?;
            raw = RawTransition::CatchAll(buf.parse()?);
        } else {
            let catch_type = buf.parse()?;
            buf.parse::<Token![match]>()?;
            let pattern = buf.parse()?;

            buf.parse::<Token![=>]>()?;
            guard = Self::parse_guard(&buf)?;
            let result = buf.parse()?;
            raw = RawTransition::CatchType(catch_type, pattern, result);
        }
        Ok(Transition { guard, raw })
    }
}