use quote::quote;
use super::transition::Transition;
use syn::{parse, parse_quote, Token};
use syn::ext::IdentExt;
use syn::parse::Parse;

pub struct State {
    pub name: syn::Ident,
    pub accepting: bool,
    pub enter_trigger: Option<syn::Expr>,
    pub leave_trigger: Option<syn::Expr>,
    pub loop_trigger: Option<syn::Expr>,
    pub transitions: Vec<Transition>
}

// To token conversion
impl State {
    pub fn get_transition_handler(&self, symbols_name: &syn::Ident, sym_types: &Vec<syn::Type>) -> proc_macro2::TokenStream {
        let transitions: Vec<proc_macro2::TokenStream> = self.transitions.iter()
            .map(|x| x.get_arm(symbols_name, sym_types))
            .collect();
        let transitions = if self.transitions.is_empty() {
            quote! {}
        } else {
            quote! { #(#transitions),*, }
        };

        let name = &self.name;
        let sym_idents: Vec<syn::Ident> = (0..sym_types.len()).map(|x| Self::get_type_ident(x)).collect();
        quote! {
            #name => match &sym {
                #transitions
                #(
                    #symbols_name::#sym_idents(x) => panic!("Exhausted transitions for input {:?} of type {}", x, stringify!(#sym_types))
                ),*,
                _ => panic!("[BUG] Exhausted transitions for unknown input")
            }
        }
    }

    pub fn get_leave_handler(&self) -> proc_macro2::TokenStream {
        let nothing = parse_quote!{ {} };
        let leave_trigger = match &self.leave_trigger {
            None => &nothing,
            Some(v) => v
        };
        let leave_trigger = quote! {
            { #leave_trigger }
        };

        let inner = if let Some(_) = &self.loop_trigger {
            quote! {
                if !looping {
                    #leave_trigger
                }
            }
        } else {
            leave_trigger
        };

        let name = &self.name;
        quote! { #name => { #inner } }
    }

    pub fn get_switch_handler(&self) -> proc_macro2::TokenStream {
        let nothing = parse_quote!{ {} };
        let enter_trigger = match &self.enter_trigger {
            None => &nothing,
            Some(v) => v
        };

        let switch_enter = quote! {
            #[allow(unreachable_code)]
            {
                self._statemachine_state = state;
                { #enter_trigger }
            }
        };
        let inner = if let Some(loop_trigger) = &self.loop_trigger {
            quote! {
                if looping {
                    { #loop_trigger }
                    self._statemachine_state = state;
                } else {
                    #switch_enter
                }
            }
        } else {
            switch_enter
        };

        let name = &self.name;
        quote! { #name => { #inner } }
    }

    fn get_type_ident(i: usize) -> syn::Ident {
        // More duplication of states.rs...
        syn::Ident::new(&format!("V{}", i), proc_macro2::Span::call_site())
    }
}

// Parsing
impl State {
    pub fn new(name: syn::Ident) -> Self {
        State {
            name,
            accepting: false,
            enter_trigger: None,
            leave_trigger: None,
            loop_trigger: None,
            transitions: Vec::new()
        }
    }

    pub fn parse_transitions(&mut self, buf: parse::ParseStream) -> syn::Result<()> {
        if !buf.is_empty() {
            self.parse_transition(buf)?;
            while buf.peek(Token![,]) {
                buf.parse::<Token![,]>()?;
                self.parse_transition(buf)?;
            }
        }
        Ok(())
    }

    fn parse_transition(&mut self, buf: parse::ParseStream) -> syn::Result<()> {
        if buf.peek(Token![@]) {
            buf.parse::<Token![@]>()?;
            self.parse_trigger(buf)?;
        } else {
            self.transitions.push(Transition::parse(buf)?);
        }
        Ok(())
    }

    fn parse_trigger(&mut self, buf: parse::ParseStream) -> syn::Result<()> {
        let keyword: syn::Ident = buf.call(syn::Ident::parse_any)?;
        match keyword.to_string().as_str() {
            "enter" => {
                if !self.enter_trigger.is_none() {
                    panic!("State {} already has an enter trigger", self.name.to_string());
                }
                buf.parse::<Token![=>]>()?;
                self.enter_trigger = Some(buf.parse()?);
            },
            "leave" => {
                if !self.leave_trigger.is_none() {
                    panic!("State {} already has a leave trigger", self.name.to_string());
                }
                buf.parse::<Token![=>]>()?;
                self.leave_trigger = Some(buf.parse()?);
            },
            "loop" => {
                if !self.loop_trigger.is_none() {
                    panic!("State {} already has a loop trigger", self.name.to_string());
                }
                buf.parse::<Token![=>]>()?;
                self.loop_trigger = Some(buf.parse()?);
            },
            v => panic!("Expected `enter`, `leave`, or `loop`; got `{}`", v)
        }
        Ok(())
    }
}