use crate::util;
use quote::quote;
use super::state::State;
use syn::{braced, bracketed, parse, parse_quote, Token};

pub struct States {
    pub name: syn::Ident,
    pub types: Vec<syn::Type>,
    pub states: Vec<State>
}

// To token conversion
impl States {
    pub fn to_tokens(&self, name: &syn::Ident, vis: &syn::Visibility) -> proc_macro2::TokenStream {
        let states_enum = self.get_states(vis);
        let symbol_enum = self.get_symbols(name);
        let state_switcher = self.get_state_switcher();
        let machine_acceptor = self.get_machine_acceptor();
        let consumer = self.get_consumer(name);

        quote! {
            #states_enum
            #symbol_enum
            impl #name {
                #state_switcher
                #machine_acceptor
                #consumer
            }
        }
    }

    fn get_states(&self, vis: &syn::Visibility) -> proc_macro2::TokenStream {
        let state_idents: Vec<syn::Ident> = self.states.iter()
            .map(|x| x.name.clone())
            .collect();

        let states_name = &self.name;
        let accepting = self.get_accepting_impl(states_name);
        let starting = &self.states[0].name;
        quote! {
            #[derive(PartialEq)]
            #vis enum #states_name {
                #(#state_idents),*
            }

            impl Default for #states_name {
                fn default() -> #states_name {
                    #states_name::#starting
                }
            }

            #accepting
        }
    }

    fn get_accepting_impl(&self, states_name: &syn::Ident) -> proc_macro2::TokenStream {
        let accepting_idents: Vec<syn::Ident> = self.states.iter()
            .filter(|x| x.accepting)
            .map(|x| x.name.clone())
            .collect();
        let accepting_idents = if accepting_idents.is_empty() {
            quote! {}
        } else {
            quote! { #(#states_name::#accepting_idents => true),*, }
        };

        quote! {
            impl #states_name {
                pub fn is_accepting(&self) -> bool {
                    match self {
                        #accepting_idents
                        _ => false
                    }
                }
            }
        }
    }

    fn get_symbols(&self, name: &syn::Ident) -> proc_macro2::TokenStream {
        let sym_name = Self::get_symbols_name(name);

        let mut punc = syn::punctuated::Punctuated::<syn::Variant, syn::token::Comma>::new();
        for (i, ty) in self.types.iter().enumerate() {
            let ident = Self::get_type_ident(i);
            punc.push(parse_quote!{ #ident(#ty) });
        }

        let converting = self.get_symbol_conversions(&sym_name);
        quote! {
            #[derive(Debug)]
            enum #sym_name {
                #punc
            }

            #converting
        }
    }

    fn get_symbol_conversions(&self, sym_name: &syn::Ident) -> proc_macro2::TokenStream {
        let mut convs = Vec::new();
        for (i, ty) in self.types.iter().enumerate() {
            let ident = Self::get_type_ident(i);
            convs.push(quote! {
                impl From<#ty> for #sym_name {
                    fn from(val: #ty) -> Self {
                        #sym_name::#ident(val)
                    }
                }
            });
        }
        quote! { #(#convs)* }
    }

    fn get_state_switcher(&self) -> proc_macro2::TokenStream {
        let states_name = &self.name;
        let leave_handlers: Vec<proc_macro2::TokenStream> = self.states.iter()
            .map(|x| x.get_leave_handler())
            .collect();
        let state_handlers: Vec<proc_macro2::TokenStream> = self.states.iter()
            .map(|x| x.get_switch_handler())
            .collect();
        quote! {
            fn reset(&mut self, state: #states_name) {
                let looping = state == self._statemachine_state;
                match &self._statemachine_state {
                    #(#states_name::#leave_handlers),*
                }
                match &state {
                    #(#states_name::#state_handlers),*
                }
            }
        }
    }

    fn get_machine_acceptor(&self) -> proc_macro2::TokenStream {
        quote! {
            fn is_accepting(&self) -> bool {
                self._statemachine_state.is_accepting()
            }
        }
    }

    fn get_consumer(&self, name: &syn::Ident) -> proc_macro2::TokenStream {
        let sym = Self::get_symbols_name(name);
        let states_name = &self.name;
        let state_handlers: Vec<proc_macro2::TokenStream> = self.states.iter()
            .map(|x| x.get_transition_handler(&sym, &self.types))
            .collect();
        quote! {
            fn consume<T: Into<#sym>>(&mut self, val: T) {
                use #states_name::*;
                let sym: #sym = val.into();
                #[allow(unreachable_code)]
                {
                    let result: #states_name = match &self._statemachine_state {
                        #(#states_name::#state_handlers),*
                    };
                    self.reset(result);
                }
            }
        }
    }

    fn get_type_ident(i: usize) -> syn::Ident {
        syn::Ident::new(&format!("V{}", i), proc_macro2::Span::call_site())
    }

    fn get_symbols_name(name: &syn::Ident) -> syn::Ident {
        let sym_name = "_statemachine_symbols_".to_owned() + &name.to_string();
        syn::Ident::new(&sym_name, proc_macro2::Span::call_site())
    }
}

// Parsing
impl States {
    pub fn parse_transitions(&mut self, buf: parse::ParseStream) -> syn::Result<()> {
        if !buf.is_empty() {
            self.parse_transition_block(buf)?;
            while buf.peek(Token![,]) {
                buf.parse::<Token![,]>()?;
                self.parse_transition_block(buf)?;
            }
        }
        Ok(())
    }

    pub fn find_create_state_index(&mut self, name: syn::Ident) -> usize {
        match self.states.iter().enumerate().find(|(_, x)| x.name == name) {
            None => {
                self.states.push(State::new(name));
                self.states.len() - 1
            },
            Some((i, _)) => i
        }
    }

    fn parse_transition_block(&mut self, buf: parse::ParseStream) -> syn::Result<()> {
        let state_name: syn::Ident = buf.parse()?;
        let state = self.find_create_state_index(state_name);
        let state = &mut self.states[state];
        buf.parse::<Token![=>]>()?;

        let transitions;
        braced!(transitions in buf);
        state.parse_transitions(&transitions)?;
        Ok(())
    }
}

impl parse::Parse for States {
    fn parse(buf: parse::ParseStream) -> syn::Result<Self> {
        buf.parse::<Token![enum]>()?;
        let state_enum_name = buf.parse()?;

        util::parse_keyword(&buf, "consumes")?;
        let mut consume_types: Vec<syn::Type> = Vec::new();
        {
            let type_list;
            bracketed!(type_list in buf);

            consume_types.push(type_list.parse()?);
            while type_list.peek(Token![,]) {
                type_list.parse::<Token![,]>()?;
                consume_types.push(type_list.parse()?);
            }
        }

        // We add the initial state first, so it is always at position 0
        util::parse_keyword(&buf, "from")?;
        let mut states = States {
            name: state_enum_name,
            types: consume_types,
            states: Vec::new()
        };
        states.find_create_state_index(buf.parse()?);

        if buf.peek(syn::Ident) {
            util::parse_keyword(&buf, "accepts")?;
            {
                let state_list;
                bracketed!(state_list in buf);

                let state = states.find_create_state_index(state_list.parse()?);
                states.states[state].accepting = true;
                while state_list.peek(Token![,]) {
                    state_list.parse::<Token![,]>()?;
                    let state = states.find_create_state_index(state_list.parse()?);
                    states.states[state].accepting = true;
                }
            }
        }
        buf.parse::<Token![;]>()?;

        states.parse_transitions(buf)?;
        Ok(states)
    }
}