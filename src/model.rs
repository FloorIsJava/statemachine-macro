mod state;
mod states;
mod transition;

use quote::quote;
use states::States;
use syn::{parse, parse_quote};

pub struct StateMachine {
    pub struct_item: syn::ItemStruct,
    pub states: States
}

impl StateMachine {
    pub fn to_stream(&self) -> proc_macro2::TokenStream {
        let state_struct = &self.struct_item;
        let states = self.states.to_tokens(&state_struct.ident, &state_struct.vis);
        quote! {
            #state_struct
            #states
        }
    }
}

impl parse::Parse for StateMachine {
    fn parse(buf: parse::ParseStream) -> syn::Result<Self> {
        let mut struct_item: syn::ItemStruct = buf.parse()?;
        let states: States = buf.parse()?;

        // Add state tracking field
        let ty = &states.name;
        let mut field: syn::FieldsNamed = parse_quote! {
            { _statemachine_state: #ty }
        };
        match &mut struct_item.fields {
            syn::Fields::Named(v) => {
                v.named.push(field.named.pop().unwrap().into_value());
            },
            syn::Fields::Unit => {
                struct_item.fields = syn::Fields::Named(field);
            },
            syn::Fields::Unnamed(_) => panic!("Unnamed fields are not supported in the state machine struct")
        }

        Ok(StateMachine { struct_item, states })
    }
}