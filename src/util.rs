use syn::parse;

pub fn parse_keyword(buf: &parse::ParseStream, expected: &str) -> syn::Result<()> {
    let keyword = buf.parse::<syn::Ident>()?;
    if keyword != expected {
        panic!("Expected `{}` keyword, got `{}`", expected, keyword.to_string());
    }
    Ok(())
}